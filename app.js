// dependencies
const http = require('http');
const express = require('express');
const rp = require('request-promise');
const cheerio = require('cheerio');
const bodyParser = require('body-parser');
const MessagingResponse = require('twilio').twiml.MessagingResponse;
const geocoder = require('geocoder');

// gets the geolocation from the state/city
const getGeo = (city, state) => new Promise((resolve) => {
  geocoder.geocode(`${city}, ${state}`, (err, data) => {
    const result = { lat: data.results[0].geometry.location.lat, lng: data.results[0].geometry.location.lng };
    resolve(result);
  });
});

// uses the geolocation with the spotcrime API to request data
const getRecentCrime = (city, state) => new Promise((resolve) => {
  getGeo(city, state).then((result) => {
    const options = {
      uri: `https://api.spotcrime.com/crimes.json?lat=${result.lat}&lon=${result.lng}&radius=0.05&callback=jQuery21308264608672204437`
+ '_1500265665761&key=privatekeyforspotcrimepublicusers-commercialuse-877.410.1607&_=1500265665764',
    };
    rp(options).then((data) => {
      resolve(data);
    });
  });
});

// filters most recent
const mostRecent = x => new Promise((resolve) => {
  let count = 99;
  const result = [];
  if (x.length < 7) {
    count = x.length;
  } else {
    count = 5;
  }
  for (let i = 0; i < count; i += 1) {
    result.push(`${x[i].type} - ${x[i].date}`);
  }
  if (result.length === count) { resolve(result); }
});

// filters shooting/robbery/arson crimes
const getViolent = x => new Promise((resolve) => {
  const count = 5;
  const result = [];
  for (let i = 0; i < x.length; i += 1) {
    if (x[i].type === 'Assault' || x[i].type === 'Shooting' || x[i].type === 'Robbery' || x[i].type === 'Arson') {
      result.push(`${x[i].type} - ${x[i].date}`);
    }
  }
  if (result.length >= count) { resolve(result); }
});

// scrapes neighborhoodscout.com for a crime index of city
const getCrimeIndex = (city, state) => new Promise((resolve) => {
  const options = {
    uri: `https://www.neighborhoodscout.com/${state}/${city}/crime`,
    transform: body => cheerio.load(body),
  };
  rp(options).then(($) => {
    const crimeIndex = $('.score.mountain-meadow').text();
    let crimePsm = $('#data-cr-crimes-per-sq-mile').children('tbody').children().text();
    crimePsm = crimePsm.match(/^\d+$/gm); // Array, [0] = City, [1], State
    const crime = { index: crimeIndex, psm: crimePsm };
    resolve(crime);
  });
});

const app = express();
app.use(bodyParser.urlencoded({
  extended: true,
}));

app.use(bodyParser.json());

app.post('/sms', (req, res) => {
  const msg = req.body.Body.toLowerCase();
  const city = req.body.FromCity;
  const state = req.body.FromState;
  const twiml = new MessagingResponse();

  if (msg === 'index') {
    getCrimeIndex(city, state).then((crime) => {
      twiml.message(`Crime Index is ${crime.index}.\n${city.charAt(0).toUpperCase() + city.slice(1).toLowerCase()} is safer than ${crime.index}% of other cities.\nCrimes per square mile ${crime.psm[0]}.`);
      res.writeHead(200, { 'Content-Type': 'text/xml' });
      res.end(twiml.toString());
    });
  } else if (msg === 'recent') {
    getRecentCrime(city, state).then((result) => {
      const toJson = result.substring(result.indexOf('(') + 1, result.length - 1);
      const recentCrimes = JSON.parse(toJson);
      mostRecent(recentCrimes.crimes).then((sorted) => {
        twiml.message(sorted.join(', '));
        res.writeHead(200, { 'Content-Type': 'text/xml' });
        res.end(twiml.toString());
      });
    });
  } else if (msg === 'violent') {
    getRecentCrime(city, state).then((result) => {
      const toJson = result.substring(result.indexOf('(') + 1, result.length - 1);
      const recentCrimes = JSON.parse(toJson);
      getViolent(recentCrimes.crimes).then((sorted) => {
        twiml.message(sorted.join(', '));
        res.writeHead(200, { 'Content-Type': 'text/xml' });
        res.end(twiml.toString());
      });
    });
  } else if (msg === 'police') {
    rp({ uri: `https://publicpd.us/${state}/${city}`, json: true }).then((result) => {
      twiml.message(`${result.DEPT} : ${result.PHONE}`);
      res.writeHead(200, { 'Content-Type': 'text/xml' });
      res.end(twiml.toString());
    });
  } else if (msg === 'help') {
    const helpmsg = 'text "index" to get your city\'s crime index, "recent" to get a list of recent crime.' +
    ' "violent" to see recent violent crime. "police", to find the non emergency number for your city\'s police.';
    twiml.message(helpmsg);
    res.writeHead(200, { 'Content-Type': 'text/xml' });
    res.end(twiml.toString());
  } else {
    twiml.message('No valid commands given. text "help" for commands');
    res.writeHead(200, { 'Content-Type': 'text/xml' });
    res.end(twiml.toString());
  }
});

http.createServer(app).listen(8010, () => {
  console.log('Express server listening on port 8010');
});

